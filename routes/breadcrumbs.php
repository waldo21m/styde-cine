<?php

/*
|--------------------------------------------------------------------------
| Breadcrumbs Routes
|--------------------------------------------------------------------------
*/

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// Inicio
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Inicio', route('home'));
});

// Inicio > Generos
Breadcrumbs::register('genres', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Generos', route('genres'));
});

// Inicio > Generos > [Genero]
Breadcrumbs::register('genre', function ($breadcrumbs, $genre) {
    $breadcrumbs->parent('genres');
    $breadcrumbs->push($genre->name, route('genres.show', $genre));
});

// Inicio > Generos > [Genero] > [Pelicula]
Breadcrumbs::register('movie', function ($breadcrumbs, $movie) {
    $breadcrumbs->parent('genre', $movie->genre);
    $breadcrumbs->push($movie->name, route('movies.show', $movie));
});
